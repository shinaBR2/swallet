'use strict';

const Country = use('App/Model/Country');
const _default = {
  page: 1,
  itemPerPage: 300
};

class CountryController {
  * index(request, response){
    let page = parseInt(request.input('page')) > 0 ? parseInt(request.input('page')) : _default.page;
    let itemPerPage = parseInt(request.input('itemperpage')) > 0 ? parseInt(request.input('itemperpage')) : _default.itemPerPage;

    let d = yield Country.paginate(page, itemPerPage);

    response.json({
      success: true,
      message: '',
      meta: {
        totalItem: d.meta.total,
        itemPerPage: d.meta.perPage,
        currentPage: d.meta.currentPage
      },
      sData: d.toJSON().data.map((obj) => this._parseResult(obj))
    });

    // response.json(d);
  }

  * detail(request, response){
    let d = yield Country.findOrFail(request.param('id'));

    response.json(d);
  }

  _parseResult(obj){
    return {
      id: obj.id,
      value: obj.name
    };
  }
}

module.exports = CountryController;
