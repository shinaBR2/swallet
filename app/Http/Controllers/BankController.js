'use strict';

const Bank = use('App/Model/Bank');
const _default = {
  page: 1,
  itemPerPage: 30
};

class BankController {
  * index(request, response){
    let countryId = parseInt(request.input('countryid')) > 0 ? parseInt(request.input('countryid')) : 0;
    let page = parseInt(request.input('page')) > 0 ? parseInt(request.input('page')) : _default.page;
    let itemPerPage = parseInt(request.input('itemperpage')) > 0 ? parseInt(request.input('itemperpage')) : _default.itemPerPage;
    let queryObj = {
      'country_id': countryId
    };
    let d;

    if (countryId == 0){
      yield response.badRequest({
        success: false,
        message: 'Bad request'
      });
    } else {
      d = yield Bank.query().where(queryObj).paginate(page, itemPerPage);

      yield response.json({
        success: true,
        message: '',
        meta: {
          totalItem: d.meta.total,
          itemPerPage: d.meta.perPage,
          currentPage: d.meta.currentPage
        },
        sData: d.toJSON().data.map((obj) => this._parseResult(obj))
      });
    }
  }

  _parseResult(obj){
    return {
      id: obj.id,
      fullName: obj.name_vi,
      shortName: obj.shortname_vi,
      status: obj.status
    };
  }
}

module.exports = BankController;
