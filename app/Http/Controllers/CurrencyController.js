'use strict';

const Currency = use('App/Model/Currency');
const _default = {
  page: 1,
  itemPerPage: 300
};

class CurrencyController {
  * index(request, response){
    let page = parseInt(request.input('page')) > 0 ? parseInt(request.input('page')) : _default.page;
    let itemPerPage = parseInt(request.input('itemperpage')) > 0 ? parseInt(request.input('itemperpage')) : _default.itemPerPage;

    let d = yield Currency.paginate(page, itemPerPage);

    response.json({
      success: true,
      message: '',
      meta: {
        totalItem: d.meta.total,
        itemPerPage: d.meta.perPage,
        currentPage: d.meta.currentPage
      },
      sData: d.toJSON().data.map((obj) => this._parseResult(obj))
    });

    // response.json(d);
  }

  * detail(request, response){
    let d = yield Currency.findOrFail(request.param('id'));

    response.json(d);
  }

  _parseResult(obj){
    return {
      id: obj.id,
      value: obj.symbol
    };
  }
}

module.exports = CurrencyController;
