'use strict';

const BankBranch = use('App/Model/BankBranch');
const _default = {
  page: 1,
  itemPerPage: 30
};

class BankBranchController {
  * index(request, response){
    let bankId = parseInt(request.input('bankid')) > 0 ? parseInt(request.input('bankid')) : 0;
    let page = parseInt(request.input('page')) > 0 ? parseInt(request.input('page')) : _default.page;
    let itemPerPage = parseInt(request.input('itemperpage')) > 0 ? parseInt(request.input('itemperpage')) : _default.itemPerPage;
    let queryObj = {
      'bank_id': bankId
    };
    let d;

    if (bankId == 0){
      yield response.badRequest({
        success: false,
        message: 'Bad request'
      });
    } else {
      d = yield BankBranch.query().where(queryObj).paginate(page, itemPerPage);

      yield response.json({
        success: true,
        message: '',
        meta: {
          totalItem: d.meta.total,
          itemPerPage: d.meta.perPage,
          currentPage: d.meta.currentPage
        },
        sData: d.toJSON().data.map((obj) => this._parseResult(obj))
      });
    }
  }

  _parseResult(obj){
    return {
      id: obj.id,
      address: obj.address,
      hotline: obj.hotline,
      phone: obj.phone,
      status: obj.status
    };
  }
}

module.exports = BankBranchController;
