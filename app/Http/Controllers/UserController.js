'use strict';

const Config = use('Config');
const User = use('App/Model/User');
const UserSetting = use('App/Model/UserSetting');
const NotifySetting = use('App/Model/NotifySetting');
const Validator = use('Validator');
const Database = use('Database');

class UserController {
  /**
   * Signup/Register handle function
   *
   * @param      {Object}  request   The request
   * @param      {Object}  response  The response
   * @return     {Object}  
   */
  * signup(request, response){
    const messages = {
      'required': '{{field}} is required',
      'email': 'Invalid email',
      'unique': '{{field}} is already existed',
      'min': '{{field}} minimum {{argument.0}} characters',//both {{argument[0]}} or {{argument['0']}} do not work
      'max': '{{field}} maximum {{argument.0}} characters'
    };
    const email = Validator.sanitizor.normalizeEmail(request.input('email'));
    const username = email.substring(0, email.indexOf('@'));
    const validate = yield Validator.validateAll({
      email: email,
      password: request.input('password')
    }, User.createRules, messages);

    if (validate.fails()){
      yield response.json({
        success: false,
        message: validate.messages().map((obj) => {
          return obj.message;
        })
      });
    } else {
      try {
        const transaction = yield Database.beginTransaction();
        const user = new User();

        user.username = username;
        user.email = email;
        user.password = request.input('password');
        user.status = Config.get('model.user.defaultValue.status');
        user.role = Config.get('model.user.defaultValue.role');

        yield user.insert();
        user.useTransaction(transaction);
        yield user.save();
        transaction.commit();
        const token = yield this._genToken(request, user);

        yield response.json({
          success: true,
          message: 'Signup success!!!',
          sData: {
            user: this._parseResult(user),
            token: token
          }
        });
      } catch(e) {
        response.send(e);
      }
    }
  }

  /**
   * Login handle function
   *
   * @param      {Object}  request   The request
   * @param      {Object}  response  The response
   * @return     {Object}  
   */
  * login(request, response){
    const messages = {
      'required': '{{field}} is required',
      'email': 'Invalid email',
      'unique': '{{field}} is already existed',
      'min': '{{field}} minimum {{argument.0}} characters',//both {{argument[0]}} or {{argument['0']}} do not work
      'max': '{{field}} maximum {{argument.0}} characters'
    };
    const email = Validator.sanitizor.normalizeEmail(request.input('email'));
    const password = request.input('password');
    const validate = yield Validator.validateAll({
      email: email,
      password: password
    }, User.baseRules, messages);

    if (validate.fails()){
      yield response.json({
        success: false,
        message: validate.messages().map((obj) => {
          return obj.message;
        })
      });
    } else {
      try{
        const token = yield request.auth.attempt(email, password);
        if (token){
          yield response.json({
            success: true,
            message: 'Loggin success!!!',
            sData: {
              token: token
            }
          });
        } else {
          yield response.send({
            success: false,
            message: 'Login erorr!!!'
          });
        }
      } catch (e){
        let message = 'Unknow error!!!';

        if (e.name === 'UserNotFoundException'){
          message = 'User not found!!!';
        } else if (e.name === 'PasswordMisMatchException'){
          message = 'Wrong password!!!';
        } else if (e.name === 'InvalidArgumentException'){
          message = 'Unable to veryfied this user!!!';//Can't create token
        }

        yield response.unauthorized({
          success: false,
          message: message
        });
      }
    }
  }

  /**
   * Logout handle function
   *
   * @param      {Object}  request   The request
   * @param      {Object}  response  The response
   * @return     {Object}  
   */
  * logout(request, response){
    // TODO
    // https://auth0.com/learn/refresh-tokens/
    // https://stackoverflow.com/questions/21978658/invalidating-json-web-tokens
    yield response.json({
      success: true,
      message: 'You have been logged out!'
    });
  }

  /**
   * Return settings of an user
   *
   * @param      {Object}  request   The request
   * @param      {Object}  response  The response
   * @return     {Object}
   */
  * setting(request, response){
    const user = request.authUser;
    let success = false;
    let message = '';
    let id = 0;

    if (!user){
      yield response.json({
        success: false,
        message: 'Please log in first'
      }); 
    }

    id = user.id;

    if (id <= 0){
      yield response.json({
        success: false,
        message: 'Not found User'
      });
    }

    let d = yield UserSetting.findByOrFail('user_id', id);
    let e = yield NotifySetting.query('user_id', id).fetch();

    yield response.json({
      success: true,
      message: '',
      sData: this._parseSetting(d, e)
    });

    yield response.json({
      success: success,
      message: message
    });
  }

  /**
   * Return payload for JWT from an user
   *
   * @param      {Object}  request  The request
   * @param      {Object}  user     The user model
   * @return     {string}  generated JWT token
   */
  * _genToken(request, user){
    return yield request.auth.generate(user, {
      id: user.uuid,
      username: user.username,
      email: user.email,
      status: user.status,
      role: user.role
    });
  }

  /**
   * Return formatted object of an user
   *
   * @param      {Object}  user    The user model
   * @return     {Object}  formatted object
   */
  _parseResult(user){
    return {
      id: user.uuid,
      username: user.username,
      email: user.email,
      status: user.status,
      role: user.role
    };
  }

  /**
   * Return formatted object for settings of an user
   * It contains general settings and notify settings
   *
   * @param      {Object}  obj        The user setting model
   * @param      {Object}  notifyObj  The notify setting model
   * @return     {Object}  formatted all user settings object
   */
  _parseSetting(obj, notifyObj){
    return {
      firstDateOfWeek: obj.first_date,
      autoBackup: obj.auto_backup,
      backupType: obj.backup_type,
      notify: {
        time: obj.daily_notify_time,
        data: notifyObj.toJSON().map((iObj) => {
          return {
            id: iObj.notify_kind_id,
            value: iObj.value
          };
        })
      }
    };
  }
}

module.exports = UserController;
