'use strict';

const City = use('App/Model/City');
const _default = {
  page: 1,
  itemPerPage: 300
};

class CityController {
  * index(request, response){
    let countryId = parseInt(request.input('countryid')) > 0 ? parseInt(request.input('countryid')) : 0;
    let type = parseInt(request.input('type')) > 0 ? parseInt(request.input('type')) : 0;
    let page = parseInt(request.input('page')) > 0 ? parseInt(request.input('page')) : _default.page;
    let itemPerPage = parseInt(request.input('itemperpage')) > 0 ? parseInt(request.input('itemperpage')) : _default.itemPerPage;
    let queryObj = {
      'country_id': countryId
    };
    let d;

    if (countryId == 0){
      yield response.badRequest({
        success: false,
        message: 'Bad request'
      });
    } else {
      if (type > 0){
        queryObj = {
          'country_id': countryId,
          'type': type
        };
      }
      d = yield City.query().where(queryObj).paginate(page, itemPerPage);

      yield response.json({
        success: true,
        message: '',
        meta: {
          totalItem: d.meta.total,
          itemPerPage: d.meta.perPage,
          currentPage: d.meta.currentPage
        },
        sData: d.toJSON().data.map((obj) => this._parseResult(obj))
      });
    }
  }

  _parseResult(obj){
    return {
      id: obj.id,
      name: obj.name,
      type: obj.type
    };
  }
}

module.exports = CityController;
