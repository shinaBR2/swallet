'use strict';

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route');

Route.group('apiv1', function(){
  Route.get('currencies', 'CurrencyController.index').as('currencyList');
  Route.get('currencies/:id', 'CurrencyController.detail').as('currencyDetail');
  Route.get('countries', 'CountryController.index').as('countryList');
  Route.get('countries/:id', 'CountryController.detail').as('countryDetail');
  Route.get('cities', 'CityController.index').as('cityList');
  Route.get('districts', 'DistrictController.index').as('districtList');
  Route.get('wards', 'WardController.index').as('wardList');
  Route.get('banks', 'BankController.index').as('bankList');
  Route.get('bankbranchs', 'BankBranchController.index').as('bankbranchList');

  Route.post('signup', 'UserController.signup').as('userSignup');
  Route.post('login', 'UserController.login').as('userLogin');
  Route.get('logout', 'UserController.logout').as('userLogout');
  Route.get('user/settings', 'UserController.setting').as('userSetting').middleware('auth');
}).prefix('api/v1');



/**
 * Should use for SPA's API
 */
Route.any('*', function *(request, response){
  yield response.sendView('home');
});