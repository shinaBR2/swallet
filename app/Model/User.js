'use strict';

const Config = use('Config');
const Lucid = use('Lucid');

class User extends Lucid {
  static boot(){
    super.boot();
    this.addHook('beforeCreate', 'hashPass', 'User.hashPassword');
    this.addHook('beforeCreate', 'genUUID', 'User.generateUUID');
  }

  static get hidden(){
    return ['id', 'password'];
  }

  static get createRules () { 
    return {
      email: `required|email|unique:users|min:${Config.get('model.user.validate.minEmailLength')}|max:${Config.get('model.user.validate.maxEmailLength')}`,
      password: `required|min:${Config.get('model.user.validate.minPasswordLength')}|max:${Config.get('model.user.validate.maxPasswordLength')}`,
    };
  }

  static get baseRules () { 
    return {
      email: `required|email|min:${Config.get('model.user.validate.minEmailLength')}|max:${Config.get('model.user.validate.maxEmailLength')}`,
      password: `required|min:${Config.get('model.user.validate.minPasswordLength')}|max:${Config.get('model.user.validate.maxPasswordLength')}`,
    };
  }

  getUuid(uuid){
    return uuid.toString('hex');
  }
}

module.exports = User;
