'use strict';

const Config = use('Config');
const Hash = use('Hash');
const UUIDv5 = require('uuid/v5');


const User = exports = module.exports = {};

User.hashPassword = function * (next) {
  this.password = yield Hash.make(this.password);
  yield next;
};

User.generateUUID = function * (next) {
  // Example uuid = 'a786984b-766e-52f1-82cd-e19992ce9192';
  // this.uuid should be an array like [167,134,152,75,118,110,82,241,130,205,225,153,146,206,145,146]
  let uuid = UUIDv5(this.username, Config.get('model.uuid')).replace(/-/g,'');
  this.uuid = Buffer.from(uuid, 'hex');
  yield next;
};
