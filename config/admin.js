'use strict';

const bankBranchStatus = {
  ENABLED: 1,
  DISABLED: 2,
  DELETED: 3
};

const bankStatus = {
  ACTIVE: 1,
  INACTIVE: 2,
  DELETED: 3
};

const notifyKindStatus = {
  ENABLED: 1,
  DISABLED: 2,
  DELETED: 3
};

const userActivityKindStatus = {
  ENABLED: 1,
  DISABLED: 2,
  ARCHIVED: 3
};

const userRoles = {
  USER_ROLE: 1,
  ADMIN_ROLE: 3
};

const userStatus = {
  ENABLED: 1,
  DISABLED: 2,
  DELETED: 3,
  ARCHIVED: 4
};

const _config = {
  bankBranch: {
    const: {
      status: bankBranchStatus,
      statusList: Object.keys(bankBranchStatus).map(key => bankBranchStatus[key])
    }
  },
  bank: {
    const: {
      status: bankStatus,
      statusList: Object.keys(bankStatus).map(key => bankStatus[key])
    }
  },
  notifyKind: {
    const: {
      status: notifyKindStatus,
      statusList: Object.keys(notifyKindStatus).map(key => notifyKindStatus[key]),
    },
    defaultValue: {
      status: notifyKindStatus.ENABLED
    }
  },
  userActivityKind: {
    const: {
      status: userActivityKindStatus,
      statusList: Object.keys(userActivityKindStatus).map(key => userActivityKindStatus[key])
    }
  },
  user: {
    const: {
      role: userRoles,
      roleList: Object.keys(userRoles).map(key => userRoles[key]),
      status: userStatus,
      statusList: Object.keys(userStatus).map(key => userStatus[key]),
    },
    defaultValue: {
      role: userRoles.USER_ROLE,
      status: userStatus.ENABLED
    }
  }
};

module.exports = _config;