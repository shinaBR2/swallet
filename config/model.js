'use strict';

(() => {
  const defaultCategoryType = {
    DEBT: 1,
    LOAN: 2
  };

  const dynamicCategoryType = {
    EXPENSE: 3,
    INCOME: 4
  };

  const categoryStatus = {
    ACTIVE: 1,
    INACTIVE: 2
  };

  const cityType = {
    PROVINCE: 1,
    CITY: 2
  };

  const districtType = {
    CITY: 1,
    BIG_DISTRICT: 2,
    SMALL_DISTRICT: 3,
    TOWN: 4
  };

  const transactionRepeatType = {
    NONE: 0,
    DAILY: 1,
    MONTHLY: 2,
    FIXED_DATE: 3,
    FIXED_RANGE: 4
  };

  const transactionStatus = {
    WAIT: 1,
    DONE: 2
  };

  const userSettingFirstDate = {
    MONDAY: 1,
    SUNDAY: 2
  };

  const userSettingBackupType = {
    DAILY: 1,
    WEEKLY: 2,
    MONTHLY: 3
  };

  const userGender = {
    UNKNOWN: 0,
    MALE: 1,
    FEMAIL: 2
  };

  const walletDateFormat = {
    DMY_D: 1,
    MDY_D: 2,
    YDM_D: 3,
    YMD_D: 4,
    DMY_S: 5,
    MDY_S: 6,
    YDM_S: 7,
    YMD_S: 8
  };

  const walletViewMode = {
    BALANCE: 1,
    FLOW: 2
  };

  const walletType = {
    CASH: 1,
    BANKING: 2,
    E_WALLET: 3,
    VISA: 4
  };

  const walletStatus = {
    ENABLED: 1,
    DISABLED: 2,
    ARCHIVED: 3
  };

  const wardType = {
    WARD: 1,
    VILLAGE: 2,
    TOWN: 3
  };


  const _config = (() => {
    return {
      uuid: '2e2a9af3-579d-4097-b565-6b327a120c9a',
      category: {
        const: {
          defaultType: defaultCategoryType,
          defaultTypeList: Object.keys(defaultCategoryType).map(key => defaultCategoryType[key]),
          dynamicType: dynamicCategoryType,
          dynamicTypeList: Object.keys(dynamicCategoryType).map(key => dynamicCategoryType[key]),
          status: categoryStatus,
          statusList: Object.keys(categoryStatus).map(key => categoryStatus[key]),
        },
        defaultValue: {
          status: categoryStatus.ACTIVE
        }
      },
      city: {
        const: {
          type: cityType,
          typeList: Object.keys(cityType).map(key => cityType[key]),
        }
      },
      district: {
        const: {
          type: districtType,
          typeList: Object.keys(districtType).map(key => districtType[key]),
        }
      },
      transaction: {
        const: {
          repeatType: transactionRepeatType,
          repeatTypeList: Object.keys(transactionRepeatType).map(key => transactionRepeatType[key]),
          status: transactionStatus,
          statusList: Object.keys(transactionStatus).map(key => transactionStatus[key]),
        },
        defaultValue: {
          repeatType: transactionRepeatType.NONE,
          status: transactionStatus.WAIT,
          isExpense: false,
          isExcluded: false,
          isHidden: false
        }
      },
      userSetting: {
        const: {
          firstDate: userSettingFirstDate,
          firstDateList: Object.keys(userSettingFirstDate).map(key => userSettingFirstDate[key]),
          backupType: userSettingBackupType,
          backupTypeList: Object.keys(userSettingBackupType).map(key => userSettingBackupType[key]),
        },
        defaultValue: {
          dailyNotifyTime: '20:00:00'
        }
      },
      user: {
        const: {
          gender: userGender,
          genderList: Object.keys(userGender).map(key => userGender[key])
        },
        validate: {
          maxEmailLength: 50,
          minEmailLength: 3,
          maxPasswordLength: 25,
          minPasswordLength: 8
        },
        defaultValue: {
          gender: userGender.UNKNOWN,
          subcribed: false
        }
      },
      wallet: {
        const: {
          dateFormat: walletDateFormat,
          dateFormatList: Object.keys(walletDateFormat).map(key => walletDateFormat[key]),
          viewMode: walletViewMode,
          viewModeList: Object.keys(walletViewMode).map(key => walletViewMode[key]),
          type: walletType,
          typeList: Object.keys(walletType).map(key => walletType[key]),
          status: walletStatus,
          statusList: Object.keys(walletStatus).map(key => walletStatus[key])
        },
        defaultValue: {
          viewMode: walletViewMode.BALANCE,
          type: walletType.CASH,
          status: walletStatus.ENABLED
        }
      },
      ward: {
        const: {
          type: wardType,
          typeList: Object.keys(wardType).map(key => wardType[key])
        }
      }
    };
  })();

  if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = _config;
  } else {
    window.swConfig = _config;
  }
})();
